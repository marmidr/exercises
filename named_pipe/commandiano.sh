#!/bin/bash

# *****************************************************************************
#  This file is part of Commandiano commandline parser
#  (c) 2017 Mariusz Midor
#  https://bitbucket.org/mmidor/
# *****************************************************************************

myname=commandiano
fifo_req=/tmp/${myname}_req
fifo_rsp=/tmp/${myname}_rsp_$$

# create command and response fifos
[ -p $fifo_req ] || mkfifo $fifo_req;
[ -p $fifo_rsp ] || mkfifo $fifo_rsp;

# for test, uncomment the below line to enable command echoing
#fifo_rsp=$fifo_req

# -----------------------------------------------------------------------------

function clrLine()
{
   echo -ne "\e[2K\r" # clear line + carriage return
}

function clrScr()
{
   echo -ne "\ec" # clear screen
#   reset
}

function cursStoreHome()
{
   echo -ne "\e7" # save curs. pos
}

function cursGoHome()
{
   echo -ne "\e8" # restore curs. pos
   cursStoreHome
}

function cursGoLeft()
{
   echo -ne "\e[D"
}

function cursGoRight()
{
   local n=$1
   [ -z $n ] && n=1
   for (( i = 0; i < $n; i++ )); do
      echo -ne "\e[C"
   done
}

function promptForCommand()
{
    # http://wiki.bash-hackers.org/scripting/terminalcodes
    echo -ne "\e[01;35m${myname}>\e[00m "
    cursStoreHome
}

function printHist()
{
   #echo -en "\n"${hist[@]}
   echo -n "commands history:"
   local hsize=${#hist[@]}
   for (( i = 0; i < $hsize; i++ )); do
      echo -ne "\n\t> "${hist[$i]}
   done
}

function updateHist()
{
   local toadd="$1"
   local todel="$2"

   # remove old entry, add new at the end of array
   # because of confusing way array are working, best way is to rewrite it to new array
   # skipping elements with given value
   local h=()
   local hsize=${#hist[@]}
   for (( i = 0; i < $hsize; i++ )); do
      local entry=${hist[$i]}
      if [ "$entry" != "$todel" ]; then
         h[${#h[@]}]="$entry"
      fi
   done

   # append new entry at the end
   if [ ${#toadd} -gt 0 ]; then
      h[${#h[@]}]="$toadd"
   fi

   # clear orig history
   hist=()
   # copy h to hist
   hsize=${#h[@]}
   for (( i = 0; i < $hsize; i++ )); do
      hist[${#hist[@]}]=${h[$i]}
   done

   hist_idx=0
}

function getCursorX()
{
   # http://stackoverflow.com/questions/8343250/how-can-i-get-position-of-cursor-in-terminal
   echo -ne "\033[6n"            # ask the terminal for the position
   read -s -d\[ garbage          # discard the first part of the response
   read -s -d\; garbage          # discard the row part of the response
   read -s -d R crpos            # store the position in bash variable
   ((crpos -= ${#myname}+1))
   [[ $crpos -lt 0 ]] && crpos=0
#   echo "$crpos"                 # print the position
   return $crpos
}

# -----------------------------------------------------------------------------

# get params passed to the script
# to execute command in the loop, use:
# while true; do ./commandiano.sh help; sleep 1; done
params=$@
if [ "$params" ]; then
   # if started with params, pass them to the fifo and quit
   promptForCommand
   echo $params
   echo "@$fifo_rsp $params" > $fifo_req
   echo
   timeout 3 cat $fifo_rsp
   rm $fifo_rsp
   exit 0
fi

# start normal session
echo "${myname} request:  ${fifo_req}"
echo "${myname} response: ${fifo_rsp}"

# -----------------------------------------------------------------------------

# typed command or command restored from history
cmd=""
# array - history of commands
hist=()
# reverse history index (from the end; 0: end, 1: end-1)
hist_idx=0

# -----------------------------------------------------------------------------

echo "Type 'help' for help; 'h' for history, 'q' to quit"
promptForCommand

# http://stackoverflow.com/questions/10679188/casing-arrow-keys-in-bash
# https://www.tutorialspoint.com/unix/unix-basic-operators.htm
# https://unix.stackexchange.com/questions/193039/how-to-count-the-length-of-an-array-defined-in-bash
# http://stackoverflow.com/questions/22842896/bash-scripting-read-single-keystroke-including-special-keys-enter-and-space
# while true; do read -p?; echo -n $REPLY | hexdump -Cc; done

while IFS= read -sN1 key # 1 char (not delimiter), silent
do
   # catch multi-char special key sequences
   read -sN1 -t 0.0001 k1
   read -sN1 -t 0.0001 k2
   read -sN1 -t 0.0001 k3
   key+=${k1}${k2}${k3}

   #echo && echo -n "$key" | hexdump -Cc && echo

   case "$key" in
       $'\e[A') # arrow up
           hsize=${#hist[@]}
           [ $hist_idx -lt $hsize ] && ((hist_idx++))
           i=$(($hsize - $hist_idx))
           cmd=${hist[i]}
           clrLine
           promptForCommand
           echo -n "$cmd"
           ;;

       $'\e[B') # arrow down
           hsize=${#hist[@]}
           [ $hist_idx -gt 1 ] && ((hist_idx--))
           i=$(($hsize - $hist_idx))
           cmd=${hist[i]}
           clrLine
           promptForCommand
           echo -n "$cmd"
           ;;

       $'\e[D') # arrow left
           ;;

       $'\e[C') # arrow right
           ;;

       $'\e[1~'|$'\eOH'|$'\e[H') # home
#           cursGoHome
           ;;

       $'\e[4~'|$'\eOF'|$'\e[F') # end
#           cursGoHome
#           n=${#cmd} # go right n times
#           cursGoRight $n
           ;;

       $'\e[3~') # del
#           getCursorX
#           crx=$?
#           if [ "$cmd" -a $crx -gt 0 ]; then
#               c1=${cmd:0:$crx-1}
#               c2=${cmd:$crx-0:${#cmd}-$crx+1}
#               cmd=$c1$c2
#               clrLine
#               promptForCommand
#               echo -n "$cmd"
#               cursGoHome
#               cursGoRight $crx
#           fi

           # remove current cmd from history
           updateHist "" "$cmd"
           cmd=""
           clrLine
           promptForCommand
           ;;

       $'\x7f'|$'\x08') # bsp
           [ "$cmd" -a "$key" = $'\x7f' ] && echo -en "\b"
#           getCursorX
#           crx=$?
#           if [ "$cmd" -a $crx -gt 0 ]; then
#               c1=${cmd:0:$crx-2}
#               c2=${cmd:$crx-1:${#cmd}-$crx+1}
#               cmd=$c1$c2
#               clrLine
#               promptForCommand
#               echo -n "$cmd"
#           fi

           # delete last character from current $cmd
           cmdlen=${#cmd}
           if [ $cmdlen -gt 0 ]; then
               cmd=${cmd:0:$cmdlen-1}
               clrLine
               promptForCommand
               # use "$cmd" instead of $cmd to preserve cmd trailing space when printing
               echo -n "$cmd"
           fi
           ;;

       $'\e') # esc
           cmd=""
           clrLine
           promptForCommand
           ;;

       $'\f') # lf (^L)
           clrScr
           promptForCommand
           echo -n "$cmd"
           ;;

       $'\t') # tab
           ;;

       $'\n') # return
           # if cmd not empty, add cmd to history
           if [ "$cmd" ]; then
               #echo -ne "\nenter($cmd);"
               if [ "$cmd" = "q" ]; then
                  echo
                  break
               fi

               if [ "$cmd" = "h" ]; then
                  echo
                  printHist
                  echo
               else
                  # pass command to the fifo if opened
                  echo "@$fifo_rsp $cmd" > $fifo_req
                  echo
                  updateHist "$cmd" "$cmd"
                  # read entire response fifo with 3s timeout
                  timeout 3 cat $fifo_rsp
               fi

               cmd=""
               promptForCommand
           fi
           ;;

       *) # default
           echo -n "$key"
           cmd+="$key"
           ;;
   esac
done

echo "Exitting..."
command sleep 0.5
#rm $fifo_req
rm $fifo_rsp

#trap "echo Remove fifo; rm /tmp/ffi" EXIT

