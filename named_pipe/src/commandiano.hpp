/*
 * File:   commandiano.hpp
 * Author: Mariusz Midor
 * https://bitbucket.org/mmidor/
 *
 * Created on April 21, 2017, 1:39 PM
 */

#ifndef COMMANDIANO_HPP
#define COMMANDIANO_HPP

#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>

#include <functional>
#include <vector>
#include <string>
#include <thread>
#include <queue>
#include <mutex>
#include <atomic>

#ifdef __CYGWIN__
# define STDFNO(f)  f->_file
#else
# define STDFNO(f)  f->_fileno
#endif


class Commandiano
{
public:
    class Context
    {
    public:
        Context(const char *responsePath = nullptr, void *pUserArgument = nullptr)
            : pUserArgument(pUserArgument)
        {
            rspnsFd = STDFNO(stdout);

            if (responsePath && *responsePath)
                rspnsFd = open(responsePath, O_WRONLY);
        }

        ~Context()
        {
            if (rspnsFd != STDFNO(stdout))
               close(rspnsFd);
        }

        void printfResponse(const char *fmt, ...) const
        {
            va_list ap;
            va_start(ap, fmt);
            vdprintf(rspnsFd, fmt, ap); // -std=gnu++11
            va_end(ap);
        }

    public:
        /** argument passed during registration of command set or NULL */
        void *pUserArgument;

    private:
        int rspnsFd;
    };

    struct CmdHandler
    {
        using handler = std::function<void(const Context &ctx, const std::vector<std::string>&)>;
        const char *name;
        const char *help;
        handler     execute;
    };

public:
    Commandiano()
    {
    }

    ~Commandiano()
    {
        endReader();
    }

    /** start thread reading fifo */
    void start(const char *requestFifoPath)
    {
        reqFifoPath = requestFifoPath;
        readerActive = true;

        auto reader_obj = [&]()
        {
            try
            {
                commandReader();
            }
            catch (...)
            {
                fprintf(stderr, "commandiano: exception caught\n");
            }
        };
        thdReader = std::thread(reader_obj);
    }

    /** register array of command handlers (may be multiple of them) */
    template <size_t N>
    void registerCommandMap(const CmdHandler(&handlers)[N], void *pContextArgument = nullptr)
    {
        cmdHandlers.emplace_back(cmd_handler_set(handlers, N, pContextArgument));
    }

    /** received commands are processed synchronously */
    void processCommands(void)
    {
        std::lock_guard<std::mutex> lck(mtxRxCommands);

        while (!rxCommandsQue.empty())
        {
            // tokenize command line
            const char *cmdLine = rxCommandsQue.front().c_str();
            std::string respFifoPath;
            std::vector<std::string> argv;
            std::string cmdName = tokenize(cmdLine, argv, respFifoPath);

            // create context for command handler
            Context ctx(respFifoPath.c_str());

            if (cmdName == "help" || cmdName == "--help" || cmdName == "-h")
            {
                ctx.printfResponse("help, --help, -h" "\n"
                                   "\t" "Print commands help" "\n");

                // print help for all registered commands
                for (auto &cmdset : cmdHandlers)
                    for (const CmdHandler *pCmd = cmdset.pBegin; pCmd != cmdset.pEnd; ++pCmd)
                        ctx.printfResponse("%s\n%s\n", pCmd->name, pCmd->help);
            }
            else
            {
                // find command handler and call it
                bool found = false;
                for (auto &cmdset : cmdHandlers)
                {
                    ctx.pUserArgument = cmdset.pContextArgument;
                    found = found || processCmd(ctx, cmdset.pBegin, cmdset.pEnd, cmdName, argv);
                }

                if (!found)
                    ctx.printfResponse("Unknown command: '%s' \n", cmdName.c_str());
            }

            rxCommandsQue.pop();
        }
    }

    /** push command on command queue */
    void pushCommand(const char *command)
    {
        std::lock_guard<std::mutex> lck(mtxRxCommands);
        rxCommandsQue.push(command);
    }

    /** check if command contains less than expected number of arguments */
    static bool insuffParams(const Context &ctx, size_t nParams, size_t expectedParams)
    {
        if (nParams < expectedParams)
        {
            ctx.printfResponse("insufficient params provided: expected %d, got %d\n",
                expectedParams, nParams);
            return true;
        }
        return false;
    }

private:
    /** fill outArgv with command arguments, set responseFifo to output fifo path, return command name */
    std::string tokenize(const char *cmdLine, std::vector<std::string> &argv, std::string &responseFifo)
    {
        // @/tmp/rsp2041 tune c15
        if (*cmdLine == '@')
        {
            responseFifo = cmdLine + 1;
            responseFifo = responseFifo.substr(0, responseFifo.find(' '));
            cmdLine = strchr(cmdLine, ' ') + 1;
        }

        char *cmd_dupl = strdup(cmdLine);
        const char *t = strtok(cmd_dupl, " ");
        if (!t) t = "";
        std::string cmdname = t;

        t = strtok(nullptr, " ");
        while (t)
        {
            argv.emplace_back(t);
            t = strtok(nullptr, " ");
        }

        free(cmd_dupl);
        return cmdname;
    }

    /** find command handler and call it */
    bool processCmd(Context &ctx,
        const CmdHandler    *cmdBegin,
        const CmdHandler    *cmdEnd,
        const std::string   &cmdName,
        const std::vector<std::string> &cmdArgv)
    {
        bool found = false;
        for (const CmdHandler *pCmd = cmdBegin; (pCmd != cmdEnd) && !found; ++pCmd)
            if (cmdName == pCmd->name)
                pCmd->execute(ctx, cmdArgv), found = true;

        return found;
    }

    void discardFifo(int fd)
    {
        char buff[1000];

        ssize_t n = pread(fd, buff, sizeof(buff), 0);
        if (n > 0)
        {
            read(fd, buff, n);
            for (char &c: buff)
            {
                if (!c) break;
                if (c == '\n') c = ';';
            }
            printf("command '%s' [%ld B] discarded", buff, n);
        }
    }

    /** thread function */
    void commandReader(void)
    {
        const int buff_size = 1024;
        char buff[buff_size];
        int fifo_fd;

        /* create the FIFO (named pipe) */
        mkfifo(reqFifoPath.c_str(), 0666);
        printf("Fifo %s created - wait for data\n", reqFifoPath.c_str());
        /* open, read, and display the message from the FIFO */
        fifo_fd = open(reqFifoPath.c_str(), O_RDONLY);

        if (fifo_fd != -1)
        {
            discardFifo(fifo_fd);

            for (; readerActive;)
            {
                ssize_t rbytes = read(fifo_fd, buff, buff_size);
                if (rbytes > 0)
                {
                    char *lf = strrchr(buff, '\n');
                    if (lf) *lf = '\0';
                    printf("Received command: '%s'\n", buff);

                    std::lock_guard<std::mutex> lck(mtxRxCommands);
                    rxCommandsQue.push(buff);
                }
                std::this_thread::sleep_for(std::chrono::milliseconds(100));
            }

            close(fifo_fd);
        }
        else
        {
            printf("Opening fifo %s failed\n", reqFifoPath.c_str());
        }

        /* remove the FIFO */
        unlink(reqFifoPath.c_str());
        printf("Fifo %s closed\n", reqFifoPath.c_str());
    }

    void endReader(void)
    {
        if (readerActive)
        {
            readerActive = false;
            if (thdReader.joinable())
                thdReader.join();
        }
    }

private:
    struct cmd_handler_set
    {
        cmd_handler_set(const CmdHandler* pFirst, size_t count, void *pContextArgument)
            : pBegin(pFirst), pEnd(pFirst + count), pContextArgument(pContextArgument) {}

        const CmdHandler* pBegin;
        const CmdHandler* pEnd;
        void *            pContextArgument;
    };

    using cmd_handlers = std::vector<cmd_handler_set>;
    using rxcmds = std::queue<std::string>;

    /** vector of registered list of command handlers */
    cmd_handlers    cmdHandlers;
    /** thread reading the input fifo */
    std::thread     thdReader;
    /** reader thread keep-alive flag */
    std::atomic_bool readerActive;
    /** request fifo path */
    std::string     reqFifoPath;
    /** queue of received commandlines ready to be processed */
    rxcmds          rxCommandsQue;
    std::mutex      mtxRxCommands;
};

#endif /* COMMANDIANO_HPP */

