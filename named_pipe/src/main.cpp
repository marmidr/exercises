/*
 * File:   main.cpp
 * Author: Mariusz Midor
 *
 * Created on April 21, 2017, 9:06 AM
 */

#include "commandiano.hpp"

static volatile bool request_quit;

static const Commandiano::CmdHandler commands[] ={
    {
        "exit",
        "\t" "Usage: exit" "\n"
        "\t" "Quits the application",
        [](const Commandiano::Context &ctx, const std::vector<std::string>&) {
            request_quit = true;
        }
    },
    {
        "print",
        "\t" "Usage: print" "\n"
        "\t" "Print all tokens from received commandline",
        [](const Commandiano::Context &ctx, const std::vector<std::string>& argv) {
            for (const auto &a : argv)
                ctx.printfResponse("\tToken: %s\n", a.c_str());
        }
    },
    {
        "beep",
        "\t" "Usage: beep <ms>" "\n"
        "\t" "Beep for specified amount of time",
        [](const Commandiano::Context &ctx, const std::vector<std::string>& argv) {
            if (Commandiano::insuffParams(ctx, argv.size(), 1)) return;

            int timems = atoi(argv[0].c_str());
            ctx.printfResponse("beep(%d)!\n", timems);
        }
    },
};


// http://stackoverflow.com/questions/2784500/how-to-send-a-simple-string-between-two-programs-using-pipes

volatile bool monitor_cmd_fifo;
const char * commandianofifo = "/tmp/commandiano_req";

void write_fifo(void)
{
    int fd;
    auto writecmd = [&](const char *cmd) {
        write(fd, cmd, strlen(cmd) + 1);
    };

    monitor_cmd_fifo = true;

    /* write to the FIFO */
    fd = open(commandianofifo, O_WRONLY);
    if (fd != -1)
    {
        printf("Fifo opened - write data\n");

        writecmd("tune c123");
        usleep(1000);
        writecmd("mute 1");
        usleep(1000);
        writecmd("help");
        usleep(1000);
        writecmd("not-a-command");
        usleep(1000);

        close(fd);
    }
    else
    {
        printf("open fifo failed\n");
    }

    sleep(1);
    monitor_cmd_fifo = false;
}

void read_fifo(void)
{
#define MAX_BUF 1024
    int fd;
    char buf[MAX_BUF];

    /* create the FIFO (named pipe) */
    mkfifo(commandianofifo, 0666);
    /* open, read, and display the message from the FIFO */
    fd = open(commandianofifo, O_RDONLY);
    printf("Fifo created - wait for data\n");

    if (fd != -1)
    {
        for (; monitor_cmd_fifo;)
        {
            ssize_t readb = read(fd, buf, MAX_BUF);
            if (readb > 0)
            {
                char *lf = strrchr(buf, '\n');
                if (lf) *lf = '\0';
                printf("\nReceived: %s\n", buf);
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }

        close(fd);
    }
    else
    {
        printf("create fifo failed\n");
    }

    /* remove the FIFO */
    unlink(commandianofifo);
    printf("Fifo closed\n");
}

void test_producer_consumer(void)
{
    std::thread consumer{read_fifo};
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    std::thread producer{write_fifo};
    consumer.join();
    producer.join();
}

void test_consumer(void)
{
    monitor_cmd_fifo = true; // from terminal: echo mute 0 > myfifo
    std::thread consumer{read_fifo};
    std::this_thread::sleep_for(std::chrono::seconds(10));
    monitor_cmd_fifo = false;
    consumer.join();
}

void test_commandiano(void)
{
    Commandiano cmd;

    cmd.registerCommandMap(commands, nullptr);
    cmd.start(commandianofifo);
    printf("Command line parser is running\n");

    for (int i = 0; i < 60 * 5; i++) // 60s
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
        cmd.processCommands();
        if (request_quit)
            break;
    }
}

/*
 *
 */
int main(int argc, char** argv)
{
    //   test_producer_consumer();
    //   test_consumer();
    test_commandiano();
    return 0;
}

