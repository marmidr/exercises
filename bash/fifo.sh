
# http://stackoverflow.com/questions/25900873/write-and-read-from-a-fifo-from-two-different-script
[ -p /tmp/ffi ] || mkfifo /tmp/ffi;

rdfifo()
{
	while true; do
		read -r line < /tmp/ffi
		if [ -n ${line} ]; then
			echo ${line}.
		fi
		command sleep 0.4
	done 
}

rdfifo &
# https://bash.cyberciti.biz/guide/Putting_functions_in_background
reader_pid=$!

for i in `seq 1 5`; do
	echo ${i} > /tmp/ffi
	command sleep 1
done

echo Kill rader funtion
kill $reader_pid
#trap "echo Remove fifo; rm /tmp/ffi" EXIT
echo Remove fifo
rm /tmp/ffi

