#!/bin/sh
# Pre-commit hook for git which removes trailing whitespace, converts tabs to spaces, 
# and append trailing empy line if missing

# based on https://gist.github.com/torsten/3706787#file-fix-whitespace-sh
# 2016-08-03

# find all changed files
changed_files=`git diff --name-only HEAD |                      # Find all changed files
               sed '/Generated/d' |                             # Ignore generated files
               egrep '(\.(c|cpp|h|hpp)$)|^CMakeLists.txt$' |    # Only process .c/.h
               uniq`                                            # Remove duplicate files

# Change field separator to newline so that for correctly iterates over lines
IFS=$'\n'

# Count modified files
files_to_check=0
for FILE in $changed_files ; do
    let files_to_check=files_to_check+1
done

# Modify when needed
files_fixed=0
for FILE in $changed_files ; do
    echo "Checking whitespace in $FILE ..." >&2
    # sed ... "964251495 1547 __diff.txt" -> "964251495"
    chsum_before=$(cksum $FILE | sed -r 's/[^0-9]+.*//g')
    # Replace tabs with four spaces
    sed -i -e 's/\t/    /g' "$FILE"
    # Strip trailing whitespace
    sed -i -e 's/[[:space:]]*$//' "$FILE"
    # Add trailing empty line if missing
    lastchar=$(tail -c1 "$FILE")
    if [ '' != "$lastchar" ]; then echo '' >> "$FILE"; fi
    chsum_after=$(cksum $FILE | sed -r 's/[^0-9]+.*//g')
    # Add file to git index again
    git add "$FILE"
    if [ $chsum_after != $chsum_before ]; then let files_fixed=files_fixed+1; fi
done

echo "Fixed files: $files_fixed / $files_to_check"
