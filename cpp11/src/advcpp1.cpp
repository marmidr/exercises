/*
 *  sages.com.pl - zaawansowany c++
 *  2017-06-19
 */

#include <memory>
#include <iostream>
#include <vector>
#include <algorithm>
#include <thread>
#include <typeinfo>
#include <type_traits>
#include <random>
#include <codecvt>
#include <iterator>
#include <stdint.h>
#include <unordered_map>
//#include <filesystem> //cpp17

/**
 * rozwój standardu, postep:
 *      https://isocpp.org/std/status
 * wskazówki, rady co robić czego nie
 *      http://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines
 * bezpieczeństwo kodu, stabilność
 *      https://www.securecoding.cert.org/confluence/pages/viewpage.action?pageId=637 -
 * YT dla architektów
 *      https://www.infoq.com/
 * code::dice conference
 *      https://www.youtube.com/channel/UCU0Rt8VHO5-YNQXwIjkf-1g
 * The C++ library for the STM32
 *      https://github.com/andysworkshop/stm32plus
 */

// erase - remove idiom, by uniknąć realokacji pamięci
// https://en.wikipedia.org/wiki/Erase%E2%80%93remove_idiom
// https://en.wikibooks.org/wiki/More_C%2B%2B_Idioms

// using vs typedef - using daje sie szablonować



// -----------------------------------------------------------------------------

class X1
{
public:
    int a,b,c;
    X1(): a{0}, b{1}, c{} {}
};

class X2
{
public:
    int a,b,c;
};

struct Y
{
    Y(std::initializer_list<int> lst)
    {
        for (const auto v : lst)
            std::cout << v << "\n";
    }
};

void types()
{
    auto a = 1;
    auto b = 120UL;
    auto c = 123'456;
    auto z = 123.45;
    decltype(a) d; // typ d == typ a
    int e{}; // e == 0
    int f{456}; // e == 456
//    int g{z}; // error, double->int causes precision lost
    decltype(a + z) az; //az -> double

    X1 x1;
    X2 x2 = {3,4,5}; // inicjalizacja agregatowa - konstruktor zbędny
}

// -----------------------------------------------------------------------------

int min1(std::initializer_list<int> lst)
{
    int m = 0;
    for (auto v : lst)
        if (m < v)
            m = v;

    return m;
}

template <typename T>
T min2(std::initializer_list<T> lst) // c++11
{
    T m = 0;
    for (auto v : lst)
        if (m < v)
            m = v;

    return m;
}

auto min3(std::initializer_list<auto> lst) // c++17
{
    auto m = 0;
    for (auto v : lst)
        if (m < v)
            m = v;

    return m;
}

template<typename T, typename W> // c++11
auto min4(T a, W b) -> decltype(a+b) // dodawanie TYPOW, z char i long zwroci long
{
    return a < b ? a : b;
}

auto min5(auto a, auto b) -> decltype(a+b) // c++17
{
    return a < b ? a : b;
}

// -----------------------------------------------------------------------------

template<int A>
struct fract1 // c++98
{
    enum {
        val = fract1<A - 1>::val * A, // rekurencja
        val10 = val*10
    };
};

template<>
struct fract1<0> // specjalizacja - warunek stopu
{
    enum {
        val = 1
    };
};


template<int A>
struct fract2 // c++03
{
    const static int val = fract2<A - 1>::val * A; // rekurencja;
};

template<>
struct fract2<0> // specjalizacja - warunek stopu
{
    const static int val = 1;
};

template<int A>
struct fract3 // c++11
{
    constexpr static int val = fract3<A - 1>::val * A; // rekurencja;
};

template<>
struct fract3<0> // specjalizacja - warunek stopu
{
    constexpr static int val = 1;
};

// -----------------------------------------------------------------------------

constexpr double foo9(const int a) // c++11
{
    // c++11 - tylko proste wyrażenia
    // c++14 i nowsze - pełne wyrażenia
    return a * 9.81;
}

// -----------------------------------------------------------------------------

template<int A>
struct fibo
{
    static_assert(A >= 0, "err");
    constexpr static int val = fibo<A - 2>::val + fibo<A - 1>::val; // rekurencja;
};

template<>
struct fibo<0> // specjalizacja - warunek stopu
{
    constexpr static int val = 1;
};

template<>
struct fibo<1> // specjalizacja - warunek stopu
{
    constexpr static int val = 1;
};

// -----------------------------------------------------------------------------

template <typename T>
void ident1(const T& t)
{
    std::cout << __PRETTY_FUNCTION__ << "\n"; // void ident1(const T&) [with T = int]

    if (std::is_integral<T>::value)
    {
        std::cout << "int=" << t << "\n";
    }
    else if (std::is_floating_point<T>::value)
    {
        if (std::is_same<float, T>::value)
            std::cout << "flo=" << t << "\n";
        else
            std::cout << "dbl=" << t << "\n";
    }
}

namespace detail {

template<typename T>
struct ident2_impl
{
    constexpr static char const  * value = "unknown";
};

template<>
struct ident2_impl<int>
{
    constexpr static char const  * value = "int";
};

} // namespace

template<typename T>
struct ident2
{
    constexpr static const char * value = detail::ident2_impl<T>::value;
};

// 5 kostek do kości, losujemy,
// wypisujemy: pair, triple, strit, four

struct Dices
{
    std::vector<uint8_t> dices;

    void play()
    {
        std::random_device rd;
        std::uniform_int_distribution<uint8_t> dist('1', '6');
        std::mt19937 gen(rd());
        dices.resize(5);
        std::generate(dices.begin(), dices.end(), [&]() { return dist(gen); });

        std::cout << "Dices are: ";
        std::copy(dices.cbegin(), dices.cend(),
            std::ostream_iterator<uint8_t>(std::cout, ", "));
        std::cout << "\n";
    }

    bool ispair() const
    {
        auto d = dices;
        std::sort(d.begin(), d.end());
        return std::adjacent_find(d.cbegin(), d.cend()) != d.cend();
    }

    bool istriple() const
    {
        auto d = dices;
        std::sort(d.begin(), d.end());
        return d[0] == d[2] or
               d[1] == d[3] or
               d[2] == d[4];
    }

    bool isfour() const
    {
        auto d = dices;
        std::sort(d.begin(), d.end());
        return d[0] == d[3] or
               d[1] == d[4];
    }

    bool isstraight() const
    {
        // 1,2,3,4,5  2,3,4,5,6
        return false;
    }

    void check()
    {
        std::boolalpha(std::cout);
        std::cout << "pair:    " << ispair() << "\n";
        std::cout << "triple:  " << istriple() << "\n";
        std::cout << "straight:" << isstraight() << "\n";
        std::cout << "four:    " << isfour() << "\n";
    }

    void remove(int x)
    {

    }
};

void lambdas()
{
    [](){ std::cout << "lambda 1" << "\n"; } ();
    auto l2 = [](){ std::cout << "lambda 2" << "\n"; };
    l2();

    class functor_x2
    {
    public:
        int operator()(int x) { return x*2; }
    };

    std::vector<int> v1{1,2,3};
    int n{};
    std::transform(v1.begin(), v1.end(), v1.begin(),
        [&n](int x){ n++; return x*3;
    });

    for (auto x : v1)
        std::cout << x << ",";
    std::cout << "\n";

    // typ lambdy
    std::function<double(int)> l3 = [](double d) { return d*0.3; };

    std::vector<std::function<void()>> lambda_vec = {
        [](){},
        []  {},
        [](){},
    };

    std::unordered_map<std::string, std::function<void()>> statemachine =
    {
        { "winter", [] { std::cout << "winter is coming! \n"; } },
        { "spring", [] { std::cout << "spring, yeah! \n"; } },
    };

    statemachine["spring"]();
}

// maszyna stanu
// boost msn meta state machine
class Kolorki
{
    std::string state;
    std::unordered_map<std::string, std::pair<std::string, std::string>> statemap =
    {
        // curr      prev  next
        {"red",     { "",       "yellow"}},
        {"yellow",  { "",       "blue"}},
        {"blue",    { "red",    "yellow"}}
    };
public:
    Kolorki()
    {
        state = "red";
        std::cout << "state:" << state << "\n";
    }

    void next()
    {
        auto nextstate = statemap[state].second;
        if (not nextstate.empty())
        {
            std::cout << "next from: " << state << " to: " << nextstate << "\n";
            state = nextstate;
        }
    }

    void prev()
    {
        auto prevstate = statemap[state].first;
        if (not prevstate.empty())
        {
            std::cout << "prev from: " << state << " to: " << prevstate << "\n";
            state = prevstate;
        }
    }
};

// -----------------------------------------------------------------------------

int main(int argc, char** argv)
{
    types();
    auto m2 = min2({1,2,3});
    auto m3 = min3({1,2,3});
    auto m4 = min4(1, 1.2);
    auto m5 = min5(1, 1.2);
    auto y = Y{1,2,3,55,7};
    std::cout << std::string(10, '=') << "\n";
    std::cout << "fract1=" << fract1<5>::val << "\n";
    std::cout << "fract2=" << fract2<5>::val << "\n";
    std::cout << "fract3=" << fract3<5>::val << "\n";
    std::cout << "foo9=" << foo9(12) << "\n";
    std::cout << "fibo1=" << fibo<1>::val << "\n";
    std::cout << "fibo5=" << fibo<5>::val << "\n";
    std::cout << std::string(10, '=') << "\n";
    ident1(11);
    ident1<float>(3.14f);
    ident1<double>(9.81);
    std::cout << std::string(10, '=') << "\n";

    Dices d;
    d.play();
    d.check();

    lambdas();

    Kolorki k;
    k.next();
    k.next();
    k.next();
    k.next();
    k.prev();
    k.next();
    return 0;
}
