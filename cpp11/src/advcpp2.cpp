/*
 *  sages.com.pl - zaawansowany c++
 *  2017-06-20
 */

#include <memory>
#include <iostream>
#include <vector>
#include <algorithm>
#include <thread>
#include <typeinfo>
#include <type_traits>
#include <random>
#include <iterator>
#include <stdint.h>
#include <unordered_map>
#include <iomanip>
#include <experimental/optional> // boost albo c++17
#include <atomic>
#include <mutex>
#include <future>

// wzorce refaktoryzacji M.Fowler :
// https://refactoring.com/catalog/

// projektowanie dużych aplikacji i rozwiazywanie problemów; nic o składni języka
// https://www.amazon.com/Large-Scale-Software-Design-John-Lakos/dp/0201633620
// po polsku: http://helion.pl/ksiazki/c-projektowanie-systemow-informatycznych-vademecum-profesjonalisty-john-lakos,cpsivp.htm

// rapid JSON, rapid XML
//  http://rapidjson.org/

// -----------------------------------------------------------------------------

template <class... Types>
void countArguments(Types... tt)
{
    size_t nargs = sizeof...(tt);
    std::cout << "arg count: " << nargs << "\n";
}

// -----------------------------------------------------------------------------

template <class T1> // specjalizacja dla głowy
void showArguments(T1 a)
{
    std::cout << a << ", ";
}

template <class T1, class... Types>
void showArguments(T1 a, Types... tt)
{
    showArguments(a);
    showArguments(tt...);
}

// CRTP pattern
// unikać singletona na rzecz monostate'a lub multitona
// lexical cast: https://theboostcpplibraries.com/boost.lexical_cast

// -----------------------------------------------------------------------------

void variadic_arg()
{
    std::cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";
    countArguments(1, "my-name", false);
    showArguments(2, "u-name", true, 3.14); std::cout << "\n";
}

// -----------------------------------------------------------------------------

double calcTax(double price)
{
    return price * 1.23;
}

double calcMargin(double price)
{   // marża
    return price * 1.1;
}

double calcShipping(double price)
{
    return price + 10;
}

//template<typename ArgType, typename ...Functions>
struct FunctionDecorator
{
    FunctionDecorator(double price) : price(price) {}

    template<typename Function>
    FunctionDecorator& decorate(Function f)
    {
        price = f(price);
        return *this;
    }

    double getValue() const { return price; }

private:
    double price;
};

template<typename ArgType, typename Function>
auto funappl_impl(ArgType arg, Function func)
{
    return func(arg);
}

template<typename ArgType, typename Function, typename... Functions>
auto funappl_impl(ArgType arg, Function func, Functions...funcs)
{
    return funappl_impl(funappl_impl(arg, func), funcs...);
}

template<typename ArgType, typename... Functions>
auto funcApplyer(ArgType arg, Functions... funcs)
{
    return funappl_impl(arg, funcs...);
}

// -----------------------------------------------------------------------------

// przkłady SFINAE

struct S1
{
    typedef int intType;
};

struct S2
{
   S2(){}
   S2(int x) {}
};

// c++98

template<typename T>
struct check_type_exists1
{
    // typy ponizej powinny mieć różne sizeof
    typedef int yes[1];
    typedef char no[2];

    template <typename W>
    static yes& check(typename W::intType*);

    template <typename W>
    static no& check(...);

    static const bool value = sizeof(check<T>(nullptr)) == sizeof(yes);
};

// c++11

template <typename ...TT>
using void_t = void;

template<typename T, typename = void> // dla 2 argumentow zawsze bedze ten szablon wybrany
struct check_type_exists2 : std::false_type {};

template<typename T>
struct check_type_exists2<T, void_t<typename T::intType>> : std::true_type {};

// c++17

//template<typename T>
//using check_type_exists_t = decltype(T::intType);

// -----------------------------------------------------------------------------

auto func_ala()
{
    return std::make_tuple(123, "alamakota");
}

// -----------------------------------------------------------------------------

void decorator()
{
    std::cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";
    auto fd = FunctionDecorator(1000.0);
    double price = fd.decorate(calcMargin)
                     .decorate(calcTax)
                     .decorate(calcShipping)
                     .getValue();
    std::cout << "p1: " << price << "\n";
    // applyer
    std::cout << "p2: " << funcApplyer(1000.0, calcMargin, calcTax, calcShipping) << "\n";
}

// -----------------------------------------------------------------------------

template<typename T>
void func(typename T::intType) {
    std::cout << "func:A\n";
}

template<typename T>
void func(T) {
    std::cout << "func:B\n";
}

void sfinae()
{
    std::cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";
    std::cout << std::boolalpha;
    std::cout << check_type_exists1<S1>::value <<"\n";
    std::cout << check_type_exists1<S2>::value <<"\n";
    std::cout << check_type_exists2<S1>::value <<"\n";
    std::cout << check_type_exists2<S1,S2>::value <<"\n";
    std::cout << check_type_exists2<S2>::value <<"\n";

    func<S1>(42); // A
    func<S2>(42); // B
}

// -----------------------------------------------------------------------------

struct Dice
{
    char value;

    Dice() : value(0) {}

    void roll(void)
    {
        std::random_device rd;
        std::uniform_int_distribution<uint8_t> dist('1', '6');
        std::mt19937 gen(rd());
        value = dist(gen);
    }
};

struct DiceBucket // kubek z kośćmi
{
    std::array<std::unique_ptr<Dice>, 5> dices;

    DiceBucket()
    {
        std::generate(dices.begin(), dices.end(), []()
        {
            auto d = std::make_unique<Dice>();
            return d;
        });
    }

    void roll()
    {
        for (std::unique_ptr<Dice> &d : dices)
            d->roll();
        for (auto &d : dices)
            std::cout << d->value << ", ";
        std::cout << "\n";
    }

    void sort()
    {
        std::sort(dices.begin(), dices.end(), [](auto &d1, auto &d2)
        {
            return d1->value < d2->value;
        });
    }

    bool ispair() const
    {
        auto &d = dices;
        return false;//std::adjacent_find(d.cbegin(), d.cend()) != d.cend();
    }

    bool istriple() const
    {
        auto &d = dices;
        return d[0]->value == d[2]->value or
               d[1]->value == d[3]->value;
    }

    bool isfour() const
    {
        return dices[0]->value == dices[0]->value or
               dices[0]->value == dices[0]->value;
    }

    bool isstraight() const
    {
        // 1,2,3,4,5  2,3,4,5,6
        return false;
    }

    void check()
    {
        sort();
        std::boolalpha(std::cout);
        std::cout << "pair:    " << ispair() << "\n";
        std::cout << "triple:  " << istriple() << "\n";
        std::cout << "straight:" << isstraight() << "\n";
        std::cout << "four:    " << isfour() << "\n";
    }

};

// -----------------------------------------------------------------------------

void dices_game()
{
    std::cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";
    DiceBucket dices;
    dices.roll();
}

// -----------------------------------------------------------------------------

struct Sailor
{
    static std::atomic<bool> sailor_walking;
    static std::mutex console_mtx;
    int position;

    Sailor() : position{10} {}

    void move(bool toright)
    {
        while (sailor_walking.load()) // store/load nie są konieczne, operatory są przeładowane
        {
            position += toright ? 1 : -1;

            if (position > 20)
                sailor_walking.store(false);

            if (position <= 0)
                sailor_walking.store(false);

            if (sailor_walking)
            {
                int rnd = std::random_device()();
                rnd %= 50;
                std::this_thread::sleep_for(std::chrono::milliseconds(rnd)); // + toright*10));

                std::lock_guard<std::mutex> lck(console_mtx);
                std::string s(22, '_');
                s[position] = '#';
                std::cout << s << std::endl;
            }
        }
    }
};

std::atomic<bool> Sailor::sailor_walking;
std::mutex Sailor::console_mtx;

void sailorpath()
{
    std::cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";
    Sailor::sailor_walking.store(true);
    Sailor sailor;
    std::thread thd1(&Sailor::move, &sailor, true);
    std::thread thd2(&Sailor::move, &sailor, false);

    std::this_thread::sleep_for(std::chrono::seconds(5));
    Sailor::sailor_walking.store(false);
    thd1.join();
    thd2.join();
}

// -----------------------------------------------------------------------------

void thd_work()
{
    std::cout << "Sie pracuje!\n";
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    std::cout << "Koniec pracy!\n";
}

void thread_vect()
{
    std::cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";
    std::vector<std::thread> vec;
    for(auto i = 0; i < 5; ++i) {
        vec.push_back(std::thread(thd_work));
    }
    for(auto& thr: vec) {
        thr.join();
    }
}

// -----------------------------------------------------------------------------

class X
{
public:
   X() {}
   void go(int& a)  { a += 2; }
};

void compute(int& a) {
    a += 2;
}

void thread_param()
{
    std::cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";
    X x;
    int a = 42;
    std::thread thr1(&X::go, &x, std::ref(a));
    thr1.join();

    std::thread thr2(&compute, std::ref(a));
    thr2.join();

    std::cout << a << std::endl;
}

// -----------------------------------------------------------------------------

int calculate(int a)
{
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    return a * 2;
}

void thread_future()
{
    std::cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";
    int a = 42;
    std::future<int> ftr = std::async(std::launch::async, calculate, a);
    for(int i = 0; i < 5; ++i)
    {
        auto status = ftr.wait_for(std::chrono::milliseconds(300));
        std::cout << "ftr.ready: " << (status == std::future_status::ready) << "\n";
        if (status == std::future_status::ready)
           break;
    }

    std::cout << "ftr.get()= " << ftr.get() << std::endl;
}

// -----------------------------------------------------------------------------

int main(int argc, char** argv)
{
    variadic_arg();
    decorator();
    sfinae();
//    using namespace std::experimental;
//    std::cout << is_detected<check_type_exists_t, S1>::value << "\n"; // 17+
//    auto [a, b] = func_ala(); // c++17
    dices_game();
    sailorpath();
    thread_vect();
    thread_param();
    thread_future();
    return 0;
}
