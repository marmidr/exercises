/*
 *  sages.com.pl - zaawansowany c++
 *  t.jasiukiewicz@gmail.com
 *  2017-11-27
 */

#include <memory>
#include <iostream>
#include <vector>
#include <algorithm>
#include <thread>
#include <typeinfo>
#include <type_traits>
#include <random>
#include <iterator>
#include <stdint.h>
#include <unordered_map>
#include <iomanip>
#include <experimental/optional> // boost albo c++17
#include <atomic>
#include <mutex>
#include <future>
#include <shared_mutex>
#include <queue>

using namespace std;

// -----------------------------------------------------------------------------

void work1()
{
    cout << "work" << endl;
}

void thd1()
{
    cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";

    auto t1 = thread(work1);
    auto t2 = thread(work1);
    t1.join();
    t2.join();
}

// -----------------------------------------------------------------------------

void work2(int val)
{
    cout << "work" << val << endl;
}

void thd2()
{
    cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";

    auto t1 = thread(work2, 1);
    auto t2 = thread(work2, 2);
    t1.join();
    t2.join();
}

// -----------------------------------------------------------------------------

void work3(int &val)
{
    val++;
}

void thd3()
{
    cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";

    int v = 0;
    // by default, mimo referencji, wartosc do thread zostanie przekazana przez WARTOSC !
    // dlatego trzeba uzyc std::ref
    auto t1 = thread(work3, ref(v)); // jest tez cref - nie uzywa kopii
    auto t2 = thread(work3, ref(v));
    t1.join();
    t2.join();
    cout << "v=" << v << endl;
}

// -----------------------------------------------------------------------------

struct W4
{
    using data_t = unordered_map<thread::id, int>;
    data_t data;

    void emulation(const string &msg, int val)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
        thread::id id = this_thread::get_id();
        data[id] = val+10; // thread-unsafe
        cout << "id:" << id << " " << msg << " " << val << endl;
    }

    data_t get_data() const { return data; }
};

void thd4()
{
    cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";

    W4 w1;
    W4 w2;
    string m1 = "kot z ";
    string m2 = "chesire";

    auto t1 = thread(&W4::emulation, &w1, cref(m1), 42);
    auto t2 = thread(&W4::emulation, &w2, cref(m2), 43);

    t1.join();
    t2.join();
}

void thd5()
{
    cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";

    W4 w1;
    string m1 = "kot z ";
    string m2 = "chesire";

    auto t1 = thread(&W4::emulation, &w1, cref(m1), 42);
    auto t2 = thread(&W4::emulation, &w1, cref(m2), 43);

    t1.join();
    t2.join();

    for (const auto &pr : w1.get_data())
        cout << pr.first << " -> " << pr.second << "\n";
}

// -----------------------------------------------------------------------------

void cpu_cores()
{
    cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";
    auto cpu_cores = std::thread::hardware_concurrency();
    // na niektorych systemach funkcja moze zwrocic 0
    auto num_threads = cpu_cores == 0 ? 2 : cpu_cores;
    cout << "cores: " << cpu_cores << ", threads: " << num_threads << endl;
}

// -----------------------------------------------------------------------------

void demonize()
{
    cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";
    using thd_cont_t = vector<thread>;

    auto wrk = []
    {
        this_thread::sleep_for(chrono::microseconds(500));
    };

    auto join_possible = [](thd_cont_t &thrds)
    {
        for (auto &t: thrds)
        {
            cout << "Thread " << t.get_id();
            if (t.joinable())
            {
                cout << " joinable \n";
                t.join();
            }
            else
            {
                cout << " not joinable \n";
            }
        }
    };

    constexpr static auto max_threads = 10;
    thd_cont_t thds(max_threads);

    for (auto &t : thds)
    {
        auto th = thread(wrk);
        t = move(th);
    }

    for (auto i = 0; i < max_threads; i += 2)
        thds[i].detach();

    join_possible(thds);
    cout << "wait a while..." << endl;
    this_thread::sleep_for(chrono::milliseconds(500));
    join_possible(thds);
}

// -----------------------------------------------------------------------------

void thd_guard()
{
    cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";

    enum class ThreadProperty
    {
        detach,
        join
    };

    class ThreadGuard
    {
    public:
        ThreadGuard(ThreadProperty prop, thread &&thd)
            : property(prop), thrd(move(thd)) {}

        ~ThreadGuard()
        {
            cout << "destroy thd" << endl;
            if (thrd.joinable())
            {
                if (property == ThreadProperty::join)
                    thrd.join();
            }
            else
            {
                thrd.detach();
            }
        }

        ThreadGuard(const ThreadGuard&) = delete;
        ThreadGuard(const ThreadGuard&&) = delete;
        ThreadGuard& operator = (const ThreadGuard&) = delete;
        ThreadGuard& operator = (const ThreadGuard&&) = delete;

//        template<typename... TT>
//        ThreadGuard(ThreadProperty prop, TT&&... args)
//            : property(prop), thrd(std::forward<TT>(args)...) {}

    private:
        ThreadProperty property;
        thread thrd;
    };

    auto work = []
    {
        this_thread::sleep_for(chrono::milliseconds(200));
    };

    try
    {
        ThreadGuard(ThreadProperty::join, thread(work));
        throw logic_error("uuups!");
    }
    catch (logic_error &e)
    {
        cerr << "error:" << e.what() << endl;
    }
}

// -----------------------------------------------------------------------------

void async_call()
{
    cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";

    auto f1 = []
    {
        return 42;
    };

    auto f3 = [](int &val)
    {
        val = 42;
    };

    auto ftr1 = async(launch::deferred, f1); // deferred - sychronicznie, wywolanie przy ftr1.get()
    future<int> ftr2 = async(launch::async, []{ return 42;}); // async - w osobnym watku
    future<int> ftr3 = async(f1); // compiler dependent

    for (auto *ftr : {&ftr1, &ftr2, &ftr3})
    {
        cout << ftr->get() << ", ";
    }
    cout << endl;

    int val{};
    future<void> ftr4 = async(f3, ref(val)); // compiler dependent
    cout << "f3=" << ((void)ftr4.get(), val) << endl; // call get, use val
}

// -----------------------------------------------------------------------------

void except_in_thd()
{
    cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";

    auto work = [](exception_ptr & eptr)
    {
        try
        {
            throw logic_error("uups!");
        }
        catch (...)
        {
            eptr = std::current_exception();
        }
    };

    exception_ptr thd_except;
    thread t1(work, ref(thd_except));
    t1.join();

    try
    {
        if (thd_except)
            rethrow_exception(thd_except);
    }
    catch (const logic_error &e)
    {
        cout << "error: " << e.what() << endl;
    }

    // w async() obsluga exceptiona jest wbudowana w future<>
}

// -----------------------------------------------------------------------------

void mtx1()
{
    cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";

    struct Counter
    {
        void changeCtr(int addVal)
        {
            for (auto i = 0; i < 6700; ++i)
            {
                lock_guard<mutex> lock(m);
                ctr += addVal;
            }
        }

        int getCtr() const
        {
            m.lock();
            auto c = ctr;
            m.unlock();
            return c;
        }

    private:
        mutable mutex m;
        int ctr = 0;
    };

    Counter c;

    for (auto i = 0; i < 10; ++i)
    {
        thread t1(&Counter::changeCtr, &c,  1);
        thread t2(&Counter::changeCtr, &c, -1);

        t1.join();
        t2.join();

        auto ctr = c.getCtr();
        cout << " c == 0: " << (ctr == 0 ? "true" : "false") << endl;
    }
}

// -----------------------------------------------------------------------------

void mtx2()
{
    cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";

    mutex data_mtx;

    auto get_access = [&](int val)
    {
        unique_lock<mutex> lck(data_mtx, std::defer_lock);
        if (val & 1)
            lck.lock();

        // uzyta optymalizacja obiektu zwracanego
        return lck;
    };

    auto fast_proc = []
    {
        // fast, unsafe
    };

    auto safe_proc = []
    {

    };

    unique_lock<mutex> lock{get_access(1)};

    if (lock.owns_lock())
    {
        cout << "fast" << endl;
        fast_proc();
    }
    else
    {
        cout << "safe" << endl;
        safe_proc();
    }
}

// -----------------------------------------------------------------------------

void mtx3()
{
    cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";

    // https://stackoverflow.com/questions/40207171/why-shared-timed-mutex-is-defined-in-c14-but-shared-mutex-in-c17
//    shared_mutex mtx; // nie ma w cygwinie ?
    shared_timed_mutex mtx;
    int val = 0;

    auto inc = [&]
    {
        for (int i = 0; i < 100; i++)
        {
            this_thread::sleep_for(chrono::milliseconds(10));
            lock_guard<shared_timed_mutex> l(mtx);
            val++;
        }
    };

    auto dec = [&]
    {
        for (int i = 0; i < 100; i++)
        {
            this_thread::sleep_for(chrono::milliseconds(9));
            lock_guard<shared_timed_mutex> l(mtx);
            val--;
        }
    };

    auto show = [&]
    {
        for (int i = 0; i < 100; i++)
        {
            this_thread::sleep_for(chrono::milliseconds(2));
            shared_lock<shared_timed_mutex> l(mtx);
            cout << "val: " << val << endl;
        }
    };

    thread t1(inc);
    thread t2(dec);
    thread t3(show);

    t1.join();
    t2.join();
    t3.join();

}

// -----------------------------------------------------------------------------

void condvar1()
{
    cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";

    mutex mtx;
    queue<int> data_que;
    condition_variable cv;
    bool stop = false;

    auto worker = [&](string msg)
    {
        for (;;)
        {
            unique_lock<mutex> lock(mtx); // mutex zalozony
            // sv.wait sciaga locka, wybudzenie zaklada locka. jak cond == false, z powrotem lockuje
            cv.wait(lock, [&]{ return !data_que.empty() || stop; });
            if (stop)
                break;

            // fron i pop rozdzielone, bo jak przy fron bedzie exception
            // to stan kolejki zostanie niezmieniony
            auto data = data_que.front();
            data_que.pop();
            // jak najszybciej zwalniamy kolejke dla innych watkow bo operacja cout jest wolna
            lock.unlock();
            cout << msg << " " << data << endl;
        }
    };

    auto sender = [&](int data)
    {
        lock_guard<mutex> lock(mtx);
        data_que.push(data);
        cv.notify_one();
    };

    auto shutdown = [&]
    {
        stop = true;
        cv.notify_all();
    };

    thread t1(worker, "first");
    thread t2(worker, "second");

    for (int i = 10; i < 50; i+=10)
    {
        sender(i);
        this_thread::sleep_for(chrono::milliseconds(200));
    }

    shutdown();
    t1.join();
    t2.join();

    cout << "que is empty: " << boolalpha << data_que.empty() << endl;
}

// -----------------------------------------------------------------------------

struct ball_t
{
    ball_t(int border)
        : max_xy(border)
    {
        dx = 1;
        dy = 1;

        std::random_device rd;
        std::uniform_int_distribution<uint8_t> dist(1, max_xy-2);
        std::mt19937 gen(rd());

        x = dist(gen);
        y = dist(gen);

        cout << "x:y=" << x << ":" << y << endl;
    }

    void animate()
    {
        x+=dx;

        if (x > max_xy-1)
        {
            dx = -dx;
            x+=dx;
            x+=dx;
            inc_collision();
        }

        y+=dy;

        if (y > max_xy-1)
        {
            dy = -dy;
            y+=dy;
            y+=dy;
            inc_collision();
        }
    }

    static void inc_collision()
    {
        num_collisions++;
    }

    static unsigned get_collisions()
    {
        int n = num_collisions;
        return n;
    }

    const int max_xy;
    unsigned x, y;
    int dx, dy;

    static atomic_int num_collisions;
};

struct table_t
{
    table_t(int size)
        : table_size(size)
    {
        table.resize(size*size, '.');

        for (int i = 0; i < num_balls; i++)
        {
            auto f = [this](ball_t &b, int delay)
            {
                for (;;)
                {
                    b.animate();
                    if (!ingame)
                        break;
                    this_thread::sleep_for(chrono::milliseconds(200 + delay*10));
                }

                cout << "bllthd.exit" << endl;
            };

            balls.emplace_back(ball_t(table_size));
            ball_t &b = balls.back();
            thds.emplace_back(f, ref(b), i);
        }
    }

    void clear()
    {
        fill(table.begin(), table.end(), '.');
    }

    void drawball(const ball_t &b)
    {
        table[b.y * table_size + b.x] = '*';
    }

    void draw()
    {
        clear();

        for (auto &b : balls)
            drawball(b);

        cout << "+";
        cout << string(table_size, '-');
        cout << "+" << endl;

        for (int y = 0; y < table_size; ++y)
        {
            char *line = &table[y * table_size];

            cout << "|";
            for (int x = 0; x < table_size; ++x)
                cout << line[x];

            cout << "|";

            if (y == 0)
                cout << "  Collisions: " << ball_t::get_collisions();

            cout << endl;
        }

        cout << "+";
        cout << string(table_size, '-');
        cout << "+" << endl;
    }

    void stop()
    {
        cout << "stopping" << endl;

        ingame = false;
        for (auto &t : thds)
            t.join();

        cout << "stopped" << endl;
    }

    vector<ball_t> balls;
    vector<char> table;
    vector<thread> thds;
    const int table_size;
    const int num_balls = 3;
    const int max_collisions = 10;
    volatile bool ingame = true;
};

atomic_int ball_t::num_collisions;

void game()
{
    cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";

    thread play([]
    {
        table_t table(10);

        for (;;)
        {
            cout << endl;
            cout << endl;
            if (ball_t::get_collisions() >= table.max_collisions)
                break;
            table.draw();
            this_thread::sleep_for(chrono::milliseconds(100));
        }

        table.stop();
        this_thread::sleep_for(chrono::milliseconds(5000));
        cout << "exit" << endl;
    });

    play.join();
}

// -----------------------------------------------------------------------------

int main(int argc, char** argv)
{
//    thd1();
//    thd2();
//    thd3();
//    thd4();
//    thd5();
//    cpu_cores();
//    demonize();
//    thd_guard();
//    async_call();
//    except_in_thd();
//    mtx1();
//    mtx2();
//    mtx3();
//    condvar1();
    game();
    return 0;
}
