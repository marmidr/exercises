/*
 *  sages.com.pl - zaawansowany c++
 *  2017-11-27
 */

#include <iostream>
#include <chrono>
#include <thread>
#include <utility>
#include <functional>

// -----------------------------------------------------------------------------

void worker(int a, int b)
{
    std::cout << "I'm worker(" << a << ',' << b << ").\n";
}

class Execute
{
public:
    Execute(Execute&& src) = default;

    ~Execute()
    {
        stop();
        thr.join();
    }

    // factory method
    template <typename Rep, typename Period, typename Function, typename ...Args>
    static Execute periodic(std::chrono::duration<Rep, Period> dur,
            Function *funcs, Args&&... args)
    {
        Execute e;
        e.thr = std::thread(&Execute::exe_periodic<Rep, Period, Function, Args...>,
                &e, dur, funcs, std::forward<Args>(args)...);
        return e;
    }

    // factory method
    template <typename Rep, typename Period, typename Function, typename ...Args>
    static Execute once(std::chrono::duration<Rep, Period> dur,
            Function *funcs, Args&&... args)
    {
        Execute e;
        e.thr = std::thread(&Execute::exe_once<Rep, Period, Function, Args...>,
                &e, dur, funcs, std::forward<Args>(args)...);
        return e;
    }

    void stop() {
        is_running = false;
    }

    bool is_stopped() const {
        return is_running;
    }

    void pause() {
        // TODO: ...
    }

    bool is_paused() const {
        // TODO: ...
        return false;
    }

private:
    Execute()
        : is_running{true}, thr{}
    {}

    template <typename Rep, typename Period, typename Function, typename ...Args>
    void exe_periodic(std::chrono::duration<Rep, Period> dur,
            Function *funcs, Args&&... args)
    {
        auto next_time = std::chrono::high_resolution_clock::now();
        // TODO: Detect func execution time overlapping.
        while(is_running)
        {
            next_time += dur;
            funcs(std::forward<Args>(args)...);
            std::this_thread::sleep_until(next_time);
        }
    }

    template <typename Rep, typename Period, typename Function, typename ...Args>
    void exe_once(std::chrono::duration<Rep, Period> dur,
            Function *funcs, Args&&... args)
    {
        std::this_thread::sleep_for(dur);
        funcs(std::forward<Args>(args)...);
    }

private:
    bool is_running;
    std::thread thr;
};

int main()
{
    {
        auto e = Execute::periodic(std::chrono::seconds(1), &worker, 1, 2);
        for(auto i = 0U; i < 10; ++i)
        {
            std::cout << "In main()" << std::endl;
            std::this_thread::sleep_for(std::chrono::milliseconds(200));
        }
    }
    std::cout << "End section1" << std::endl;
    {
        auto e = Execute::once(std::chrono::seconds(1), &worker, 2, 2);
        std::this_thread::sleep_for(std::chrono::seconds(2));
    }
    std::cout << "End main()" << std::endl;
}
