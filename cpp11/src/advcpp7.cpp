/*
 *  sages.com.pl - zaawansowany c++
 *  2017-11-27
 */

#include <memory>
#include <iostream>
#include <vector>
#include <algorithm>
#include <thread>
#include <typeinfo>
#include <type_traits>
#include <random>
#include <iterator>
#include <stdint.h>
#include <iomanip>
#include <experimental/optional> // boost albo c++17
#include <atomic>
#include <mutex>
#include <future>
#include <shared_mutex>
#include <queue>
#include <unordered_map>
#include <cassert>

using namespace std;
using namespace std::chrono_literals;

// -----------------------------------------------------------------------------

void once()
{
    cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";

    string s;

    // http://en.cppreference.com/w/cpp/thread/call_once
    once_flag o;
    call_once(o, [&](int x){ s = '0'+x; }, 1);
}

// -----------------------------------------------------------------------------

// zamiennik singletona, prosty i bezpieczny
struct MonoState
{
    MonoState()
    {}

    int getVal() const { return MonoState::value; }
    void setVal(int x) { MonoState::value = x; }
    size_t getId() { return reinterpret_cast<size_t>(this); }

    // moze byc duzo obiektow, ale wszystkie uzywaja jednego wspolnego zasobnu
    static int value;
};

int MonoState::value;

void ms()
{
    cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";

    MonoState m1;
    MonoState m2;
    m1.setVal(42);
    assert(m2.getVal() == m1.getVal());
    cout << "m1.id:" << m1.getId() << endl;
    cout << "m2.id:" << m2.getId() << endl;
}

// -----------------------------------------------------------------------------

// gcc optymalizacja lto (link time optimization) - scalenie kodu do jednego pliku
// -flto

// PGO: gcc optymalizacja 2 etapowa: w pierwszym etapie generuje plik z info o programie,
// potem go uzywa
// https://en.wikipedia.org/wiki/Profile-guided_optimization

// intrinsics - naglowki z funkcjami ktore przekladaja sie prosto na instrukcje CPU

// export LD_PRELOAD=~/mylib.so - zaladuj wskazana biblioteke zamiast domyslnej

void atom1()
{
    cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";

    // http://en.cppreference.com/w/cpp/atomic/atomic_flag
    atomic_flag af = ATOMIC_FLAG_INIT; // implementation-dependent
    af.test_and_set(memory_order_acquire);
    af.clear(memory_order_release);
}

// -----------------------------------------------------------------------------

int main(int argc, char** argv)
{
    once();
    ms();
    atom1();
    return 0;
}
