/*
 *  sages.com.pl - zaawansowany c++
 *  2017-11-27
 */

#include <memory>
#include <iostream>
#include <vector>
#include <algorithm>
#include <thread>
#include <typeinfo>
#include <type_traits>
#include <random>
#include <iterator>
#include <stdint.h>
#include <iomanip>
#include <experimental/optional> // boost albo c++17
#include <atomic>
#include <mutex>
#include <future>
#include <shared_mutex>
#include <queue>
#include <unordered_map>

using namespace std;

// -----------------------------------------------------------------------------

void ftr1()
{
    cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";

    future<int> f = async(launch::async, []{ return 42; });
    cout << boolalpha << f.valid() << endl;
    cout << f.get() << endl;
    cout << boolalpha << f.valid() << endl;
//    cout << f.get() << endl; // exception
//    numer sygnalu > 128 -> x - 128 --> numer sygnalu (kill --list)
}

// -----------------------------------------------------------------------------

using namespace std::chrono_literals;

void ftr2()
{
    cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";

    auto f = async(launch::async, []
    {
        this_thread::sleep_for(100ms);
        return 42;
    });

    future_status f_stat = f.wait_for(100ms);

    if (f_stat == future_status::ready)
        cout << "policzylo sie" << endl;
    else if (f_stat == future_status::deferred)
        cout << "jeszcze nie zlecony do wykonania" << endl; // prawie nie do złapania
    else
        cout << "timeout" << endl;

    (void)f.get();
}

// -----------------------------------------------------------------------------

template <typename Future>
void show_future_status(Future &ftr)
{
//    static unordered_map<future_status, string> fmap =
//    {
//        { future_status::deferred, "deferred"},
//        { future_status::ready, "ready"},
//        { future_status::timeout, "timeout"}
//    };

    future_status status;
    cout << "waiting..." << endl;
    do
    {
        status = ftr.wait_for(1s);
//        cout << fmap[status] << endl;
        cout << "..." << endl;
    } while (status != future_status::ready);
}

void ftr3()
{
    cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";

    auto delayed_exec = [](shared_future<int>& ftr)
    {
        this_thread::sleep_for(500ms);
        cout << ftr.get() << endl;
        cout << "future valid ? " << boolalpha << ftr.valid() << endl;
    };

    future<int> ftr = async(launch::async, []
    {
        this_thread::sleep_for(3s);
        return 42;
    });

    // jak od strzalu dostac future shared
//    auto ftr_s = async(launch::async, [] { return 42; }).share();

    // lambda :)
    [](){}();

    cout << "ftr.valid? " << boolalpha << ftr.valid() << endl;
    shared_future<int> s_ftr (ftr.share()); // konwersja, ftr jest teraz invalid
    cout << "ftr.valid? " << boolalpha << ftr.valid() << endl;
    cout << "s_ftr.valid? " << boolalpha << s_ftr.valid() << endl;

    thread thd1(delayed_exec, ref(s_ftr));
    thd1.detach();

    show_future_status(s_ftr);
    cout << "back in main" << endl;
}

// -----------------------------------------------------------------------------

void ftr4()
{
    cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";

//    dupa << Jureczek ;)

    using task_t = packaged_task<int(int)>;
    task_t t1{[](int a){ return a*10;}};
    auto ftr1 = t1.get_future();
    t1(5);
    cout << ftr1.get() << endl;

//    task_t t2{[](int a){ return a*20;}};
//    auto ftr2 = t2.get_future();
//    thread thd1{move(t2), 40};
//    thd1.detach();
//    cout << ftr2.get() << endl;

    // TODO: to ponizej to WIRUS :D

//    vector<task_t> tasks;
//    tasks.emplace_back(task_t([](int a){ return a+10;}));
//    tasks.emplace_back(task_t([](int a){ return a+20;}));
//    size_t choice = 0;
//    auto ftr3 = tasks[0].get_future();
//    thread(move(tasks[choice]), 10).detach();
//    cout << ftr3.get() << endl;
//
//    ftr3 = tasks[1].get_future();
//    tasks[1](30);
//    cout << ftr3.get() << endl;
}

// -----------------------------------------------------------------------------

void megastore_game()
{
    cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";

    // objective: every 5s 2 producers put 1000 elements into
    // every 3s 3 consuments gets 300 or 400
    // log every action, check maximum stored values, chceck is store empty
    // stop after 1 minute

    struct storage_t
    {
        void put(int id)
        {
            this_thread::sleep_for(chrono::milliseconds(id*500));

            for (;work;)
            {
                {
                    lock_guard<mutex> l(items_mtx);
                    cout << id << ": put 1000" << endl;
                    items.push_back(1000);
                    int sum = 0;
                    for (auto x : items) sum += x;
                    if (sum > items_max)
                        items_max = sum;

                    cout << "stan: ";
                    for (auto x : items)
                        cout << x << ", ";
                    cout << endl << endl;
                }
                this_thread::sleep_for(5s);
            }

            cout << "put " << id << " finish" << endl;
        }

        void pop(int id)
        {
            bool popextra = false;
            this_thread::sleep_for(chrono::milliseconds(id*500));

            for (;work;)
            {
                {
                    lock_guard<mutex> l(items_mtx);
                    int howmany = popextra ? 400 : 300;
                    popextra = !popextra;
                    cout << id << " pop: " << howmany << endl;

                    if (items.size())
                    {
                        if (items.back() > howmany)
                        {
                            items.back() -= howmany;
                        }
                        else
                        {
                            // no enough elements on last position
                            while (items.size() && (howmany > 0))
                            {
                                howmany -= items.back();
                                items.pop_back();

                                if (items.size() == 0)
                                {
                                    cout << "partial pop" << endl;
                                    store_empty = true;
                                }
                                else
                                {
                                    if (items.back() > howmany)
                                    {
                                        items.back() -= howmany;
                                        howmany = 0;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        cout << "store is empty" << endl;
                        store_empty = true;
                    }

                    cout << "stan: ";
                    for (auto x : items)
                        cout << x << ", ";
                    cout << endl << endl;
                }
                this_thread::sleep_for(3s);
            }

            cout << "pop " << id << " finish" << endl;
        }

        void play()
        {
            work = true;
            auto thd_put1 = thread(&storage_t::put, this, 1);
            auto thd_put2 = thread(&storage_t::put, this, 2);

            auto thd_pop1 = thread(&storage_t::pop, this, 1);
            auto thd_pop2 = thread(&storage_t::pop, this, 2);
            auto thd_pop3 = thread(&storage_t::pop, this, 3);

            this_thread::sleep_for(30s);
            work = false;

            thd_put1.join();
            thd_put2.join();
            thd_pop1.join();
            thd_pop2.join();
            thd_pop3.join();

            cout << "----------------" << endl;
            cout << "max stan: " << items_max << endl;
            cout << "pusty:    " << boolalpha << store_empty << endl;

            int sum = 0;
            for (auto x : items) sum += x;
            cout << "na koniec: " << sum << endl;
        }


        bool work;
        vector<int> items;
        mutex items_mtx;
        int items_max = 0;
        bool store_empty = false;
    };

    storage_t storage;
    storage.play();
}

// -----------------------------------------------------------------------------

struct PimplExample1
{
    PimplExample1();
    ~PimplExample1();

    // prywatny typ w klasie PimplExample1 - nie będzie konfliktów deklaracji typu
    struct InternalData;
    std::unique_ptr<InternalData> m_pData;
};

// struktura z zewnętrznej biblioteki
struct LibraryData
{
    std::string name;
};

// typ wewnętrzny jest typem z zewnętrznej biblioteki
struct PimplExample1::InternalData : public LibraryData
{
};

PimplExample1::PimplExample1()
    : m_pData(std::make_unique<InternalData>())
{
}

PimplExample1::~PimplExample1() = default;

// ---

// publiczna deklaracja zapowiadająca
struct ExternalData;

struct PimplExample2
{
    PimplExample2();
    ~PimplExample2();

    std::unique_ptr<ExternalData> m_pData;
};

// definicja struktury z zewnętrznej biblioteki
struct ExternalData
{
    std::string name;
};

PimplExample2::PimplExample2()
    : m_pData(std::make_unique<ExternalData>())
{
}

PimplExample2::~PimplExample2() = default;

// ---

void pimpl_example()
{
    PimplExample1 ex1;
    ex1.m_pData->name = "aaa";

    PimplExample2 ex2;
    ex2.m_pData->name = "bbb";
}

// -----------------------------------------------------------------------------

int main(int argc, char** argv)
{
//    ftr1();
//    ftr2();
//    ftr3();
//    ftr4();
//    megastore_game();
    pimpl_example();
    return 0;
}
