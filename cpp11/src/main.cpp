/*
 * File:   main.cpp
 * Author: mario
 *
 * Created on 1 styczeń 2014, 12:39
 */

#include <memory>
#include <iostream>
#include <vector>
#include <algorithm>
#include <chrono>
#include <thread>
#include <typeinfo>

#define STATIC_ASSERT(expr)     typedef char __statassert[(expr)?1:-1];

using namespace std;

class Domek
{
	string Nazwa;
public:
	Domek(string N) { Nazwa = N; cout << "Buduje " << N << endl; }
	~Domek() { cout << "Niszcze " << Nazwa << endl; }
	string GetName() { return Nazwa; }
};


void test_uniquepointers()
{
	cout << endl << "[[ " << __FUNCTION__ << " ]]" << endl;
	Domek d1("Parterowy");
	// http://www.youtube.com/watch?v=YeI6V2O5BFE

	unique_ptr<Domek> d2(new Domek("Górski"));
	unique_ptr<Domek> d3(d2.release());
}


void test_sharedpointers()
{
	cout << endl << "[[ " << __FUNCTION__ << " ]]" << endl;
	// http://www.youtube.com/watch?v=qUDAkDvoLas
	// http://www.youtube.com/watch?v=n10ERmH8bYk
	shared_ptr<Domek> d4 = make_shared<Domek>("Osiedlowy");
	cout << "d4 usecount:" << d4.use_count() << ", unique:" << d4.unique() << endl;
	shared_ptr<Domek> d5 = d4;
	cout << "d4 usecount:" << d4.use_count() << ", unique:" << d4.unique() << endl;
	cout << "d5 usecount:" << d5.use_count() << ", unique:" << d5.unique() << endl;
}

class Distance
{
	double	mValue; // in meters

public:
	constexpr Distance() : mValue(0) {}
	constexpr Distance(double val) : mValue(val) {}
//	~Distance() = default;

	double GetValue(void) const { return mValue; }
	operator double () const { return mValue; }
};

enum class Units : int8_t
{
    UNKNOWN,
    DISTANCE,
    TEMPERATURE,
    WEIGHT,
    POWER
};

constexpr Distance operator "" _km(long double len) { return Distance(len * 1e3); }
constexpr Distance operator "" _m (long double len) { return Distance(len * 1); }
constexpr Distance operator "" _dm(long double len) { return Distance(len * 1e-1); }
constexpr Distance operator "" _cm(long double len) { return Distance(len * 1e-2); }
constexpr Distance operator "" _mm(long double len) { return Distance(len * 1e-3); }
constexpr Distance operator "" _um(long double len) { return Distance(len * 1e-6); }

void test_literal()
{
	cout << endl << "[[ " << __FUNCTION__ << " ]]" << endl;
	// http://www.youtube.com/watch?v=2xUJTXeBZmE
	auto l0 = 15.0_km;
	Distance l1 = 15.0_m;
	Distance l2 = 15.0_dm;
	Distance l3 = 15.0_cm;
	Distance l4 = 15.0_mm;
	Distance l5 = 15.0_um;
	cout << l0 << ", "
		<< l1 << ", "
		<< l2 << ", "
		<< l3 << ", "
		<< l4 << ", "
		<< l5 << ", "
		<< endl;
}



class Joule
{
public:
	double	power;
public:
	explicit Joule(double p) : power(p) {}
	explicit Joule(long double p) : power(p) {}
	string tostring()
    {
        //return to_string(power) + "J";
        return "? J";
    }
	ostream& operator << (ostream &os) { os << tostring(); return os; }
	Joule& operator += (const Joule &a) { power += a.power; return *this; }
    Units GetUnit(void) { return Units::POWER; }
};

unique_ptr<Joule> operator "" _J(long double j) { return unique_ptr<Joule>(new Joule(j)); }
ostream& operator << (ostream &os, Joule &j) { os << j.tostring(); return os; }

void test_unit()
{
	cout << endl << "[[ " << __FUNCTION__ << " ]]" << endl;
	auto p1 = 3.14_J;
	unique_ptr<Joule> p2 = 32768.0_J;
    cout << typeid(p1).name() << endl;
    cout << typeid(*p1).name() << endl;
	cout << (*p1) << endl;
	cout << (*p2) << endl;

//	*p1 += 10.0; // error
	*p1 += *10.0_J;
	cout << *p1 << endl;
}




class OnlyStatic
{
	friend class OnlyStaticSP;
	string mName;
	void * operator new(size_t size)
	{
		OnlyStatic *p = static_cast<OnlyStatic*>(calloc(1, sizeof(OnlyStatic)));
		return p;
	}
public:
	OnlyStatic(const char *Name) { mName = Name; cout << "Create OnlyStatic '" << mName << "'" << endl; }
	~OnlyStatic() { cout << "Delete OnlyStatic '" << mName << "'" << endl; }
	void PrintName() { cout << mName << endl; }

};

class OnlyStaticSP final: public shared_ptr<OnlyStatic>
{
	void * operator new(size_t size) = delete;
public:
	OnlyStaticSP(const char *Name) : shared_ptr<OnlyStatic>(new OnlyStatic(Name)) {}
};

void test_disallow_new()
{
	cout << endl << "[[ " << __FUNCTION__ << " ]]" << endl;
	// http://www.youtube.com/watch?v=Lt0ASrloGSE
//	OnlyStatic *p_os = new OnlyStatic(); // <<----- error: forbidden
	OnlyStaticSP ossp1("OSP1");
	ossp1->PrintName();
	OnlyStaticSP ossp2("OSP2");
	ossp2 = ossp1;
	ossp2->PrintName();

//	OnlyStaticSP *p_ossp = new OnlyStaticSP("OSSP3"); // <<----- error: forbidden
}


void test_lambda()
{
	// https://www.youtube.com/watch?v=5t-_wI7nFdU

	vector<int> col {2,3,6,8,10,15,23,24,29,35,40};

	cout << "Count of multiple of " << "2" << " is " <<
		count_if(col.begin(), col.end(), [](int x) { return x % 2 == 0; }) << endl;

	int multi = 3;

	// capture external multi by value (copy)
	cout << "Count of multiple of " << multi << " is " <<
		count_if(col.begin(), col.end(), [multi](int x) { return x % multi == 0; }) << endl;

	// capture everything from outside by value (copy)
	cout << "Count of multiple of " << multi << " is " <<
		count_if(col.begin(), col.end(), [=](int x) { return x % multi == 0; }) << endl;

	// capture external sum by reference (no longer readonly)
	int sum = 0;
	for_each(col.begin(), col.end(), [&sum](int x) { sum += x; });

	// cpture everything by reference (no longer readonly)
	sum = 0;
	for_each(col.begin(), col.end(), [&](int x) -> int { sum += x; });
	cout << "The sum of elements is " << sum << endl;

	sum = 0;
	for (auto x : col) sum += x;
	cout << "The sum of elements is " << sum << endl;

}

STATIC_ASSERT(sizeof(int) == 4);
STATIC_ASSERT(sizeof(int) >= 4);

/*
 *
 */
int main(int argc, char** argv)
{
	auto * rawstr = R"==(abc\r\t)==";
	test_uniquepointers();
	test_sharedpointers();
	test_literal();
	test_unit();
	test_disallow_new();
	test_lambda();
	return 0;
}

