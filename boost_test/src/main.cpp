//
// Boost examples
//

#include <iostream>
#include <cstdlib>
#include <string>

#include <boost/variant.hpp>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/sequenced_index.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/key_extractors.hpp>

namespace bmi = boost::multi_index;
using namespace std;

// -----------------------------------------------------------------------------

typedef bmi::multi_index_container<
   std::string,
   bmi::indexed_by<
      bmi::sequenced<>,
      bmi::ordered_unique<bmi::identity<std::string>>
   >
> text_t;

void multiindex1()
{
   cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";
   // http://www.drdobbs.com/the-boost-multi-index-containers-library/184401843?pgno=1
   // https://wandbox.org/permlink/QV4HXkoGgKrwNwfl
   // https://theboostcpplibraries.com/boost.multiindex

   text_t txt;
   txt.get<0>().push_back("j");
   txt.get<0>().push_back("h");
   txt.get<0>().push_back("f");
   txt.get<0>().push_back("b");
   txt.get<0>().push_back("d");

   txt.get<1>().insert("e");
   txt.get<1>().insert("c");
   txt.get<1>().insert("i");
   txt.get<1>().insert("g");
   txt.get<1>().insert("a");

   cout << "--- ONE CONTAINER, UNSORTED VIEW ---" << endl;
   for (auto &s : txt.get<0>())
      cout << s << ", ";
   cout << endl;

   cout << "--- THE SAME CONTAINER, SORTED VIEW ---" << endl;
   for (auto &s : txt.get<1>())
      cout << s << ", ";
   cout << endl;
}

// -----------------------------------------------------------------------------

struct animal_t
{
   animal_t(const char *name, int legs)
      : name(name), legs(legs)
   {
      std::cout << "create animal  : " << name << "\n";
   }
   animal_t(const animal_t &o)
      : name(o.name), legs(o.legs)
   {
      std::cout << "create animal& : " << name << "\n";
   }
   animal_t(animal_t &&o)
      : name(o.name), legs(o.legs)
   {
      std::cout << "create animal&&: " << name << "\n";
      o.name.insert(0, "moved ");
   }
   ~animal_t()
   {
      std::cout << "destroy animal:  " << name << "\n";
   }
   std::string name;
   int         legs;
};

typedef bmi::multi_index_container<
   animal_t,
   bmi::indexed_by<
      bmi::hashed_non_unique<
         bmi::member<animal_t, std::string, &animal_t::name>
      >,
      bmi::hashed_non_unique<
         bmi::member<animal_t, int, &animal_t::legs>
      >
   >
> animal_multi_t;

void multiindex2()
{
   cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";

   {
      cout << "***\n";
      animal_multi_t animals;

      animals.insert({"cat", 4});
      animals.insert({"shark", 0});
      animals.insert({"spider", 8});

      std::cout << "count of 'cat': " << animals.count("cat") << '\n';

      const animal_multi_t::nth_index<1>::type &legs_index = animals.get<1>();
      std::cout << "count of 8 legs: " << legs_index.count(8) << '\n';
   }

   {
      cout << "***\n";
      animal_multi_t animals;

      animals.insert({"cat", 4});
      animals.insert({"shark", 0});
      animals.insert({"spider", 8});

      auto &legs_index = animals.get<1>();
      auto it = legs_index.find(4);
      legs_index.modify(it, [](animal_t & a) {
         a.name = "dog"; });

      std::cout << "count of 'dog':" << animals.count("dog") << '\n';
   }

   {
      cout << "***\n";
      animal_multi_t animals;

      animals.insert({"cat", 4});
      animals.insert({"shark", 0});
      animals.insert({"dog", 4});

      auto &legs_index = animals.get<1>();
      std::cout << "count of 4 legs: " << legs_index.count(4) << '\n';
   }
}

// -----------------------------------------------------------------------------

void f_double(double v)
{
   std::cout << v << std::endl;
}

void f_char(char v)
{
   std::cout << v << std::endl;
}

void f_bool(bool v)
{
   std::cout << v << std::endl;
}

// https://stackoverflow.com/questions/13750838/how-to-implement-a-boostvariant-derived-class

class MyUnion
{
public:
   template <class T>
   MyUnion & operator=(T value)
   {
      mStorage = value;
      return *this;
   }

   //MyUnion & operator=(double d)       { mStorage = d; return *this; }
   //MyUnion & operator=(char c)         { mStorage = c; return *this; }
   //MyUnion & operator=(std::string s)  { mStorage = s; return *this; }

   template <class T>
   operator T() const
   {
      return boost::get<T>(mStorage);
   }

   //    operator double()       const { return boost::get<double>(mStorage); }
   //    operator char()         const { return boost::get<char>(mStorage); }
   //    operator std::string()  const { return boost::get<std::string>(mStorage); }
private:
   boost::variant<double, char, std::string> mStorage;
};

void variant()
{
   cout << "\n===[ " << __PRETTY_FUNCTION__ << " ]===\n";
   MyUnion u;
   u = 6.28;
   f_double(u);
   u = 'B';
   f_char(u);

   //  f_bool(u); // <-- compile error :-))

   /*
   boost::variant<double, char, std::string> v;
   v = 3.14;
   f_double(boost::get<double>(v));
   v = 'A';
   f_char(boost::get<char>(v));
   v = "Boost";
    */
}

// -----------------------------------------------------------------------------

int main()
{
   multiindex1();
   multiindex2();
   variant();
}
