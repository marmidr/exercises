#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=MinGW-7.2-Windows
CND_ARTIFACT_DIR_Debug=dist/Debug/MinGW-7.2-Windows
CND_ARTIFACT_NAME_Debug=boost_test
CND_ARTIFACT_PATH_Debug=dist/Debug/MinGW-7.2-Windows/boost_test
CND_PACKAGE_DIR_Debug=dist/Debug/MinGW-7.2-Windows/package
CND_PACKAGE_NAME_Debug=boosttest.tar
CND_PACKAGE_PATH_Debug=dist/Debug/MinGW-7.2-Windows/package/boosttest.tar
# Release configuration
CND_PLATFORM_Release=MinGW-7.2-Windows
CND_ARTIFACT_DIR_Release=dist/Release/MinGW-7.2-Windows
CND_ARTIFACT_NAME_Release=boost_test
CND_ARTIFACT_PATH_Release=dist/Release/MinGW-7.2-Windows/boost_test
CND_PACKAGE_DIR_Release=dist/Release/MinGW-7.2-Windows/package
CND_PACKAGE_NAME_Release=boosttest.tar
CND_PACKAGE_PATH_Release=dist/Release/MinGW-7.2-Windows/package/boosttest.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
