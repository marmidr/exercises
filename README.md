# README #


### What is this repository for? ###

Collection of small projects purposing exercises from vast area of C++ programming
 
* C++11 features
* Linux named pipes (fifo)
* OpenMP examples

### How do I get set up? ###

Use Netbeans 8 for Cpp to open and run projects

### Troubleshooting

    $ git clone https://mmidor@bitbucket.org/mmidor/exercises.git
    Cloning into 'exercises'...
    fatal: unable to access 'https://mmidor@bitbucket.org/mmidor/exercises.git/': Could not resolve host
    : bitbucket.org

export https_proxy=10.224.189.141:8080

    $ git clone https://mmidor@bitbucket.org/mmidor/exercises.git
    Cloning into 'exercises'...
    remote: Counting objects: 41, done.
    remote: Compressing objects: 100% (38/38), done.
    Uremote: Total 41 (delta 17), reused 0 (delta 0)
    Unpacking objects: 100% (41/41), done.
    Checking connectivity... done.
    