/*
 * File:   main.cpp
 * Author: mario
 *
 * Created on 19 września 2015, 19:09
 */

#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

// https://computing.llnl.gov/tutorials/openMP/exercise.html

void hello()
{
    printf("\n~~~~~~~~ hello\n");
    int nthreads, tid;

    /* Fork a team of threads giving them their own copies of variables */
#pragma omp parallel private(nthreads, tid)
    {
        /* Get thread number */
        tid = omp_get_thread_num();
        printf("Hello World from thread = %d\n", tid);

        /* Only master thread does this */
        if (tid == 0)
        {
            nthreads = omp_get_num_threads();
            printf("Number of threads = %d\n", nthreads);
        }
    } /* All threads join master thread and disband */
}

// -----------------------------------------------------------------------------

/*
 * DESCRIPTION:
 *   OpenMP Example - Loop Work-sharing - C/C++ Version
 *   In this example, the iterations of a loop are scheduled dynamically
 *   across the team of threads.  A thread will perform CHUNK iterations
 *   at a time before being scheduled for the next CHUNK of work.
 * AUTHOR: Blaise Barney  5/99
 * LAST REVISED: 04/06/05
 */
void workshare1()
{
    printf("\n~~~~~~~~ workshare1\n");
    const int CHUNKSIZE = 10;
    const int N = 100;

    int nthreads, tid, i, chunk;
    float a[N], b[N], c[N];

    /* Some initializations */
    for (i = 0; i < N; i++)
        a[i] = b[i] = i * 1.0;

    chunk = CHUNKSIZE;

#pragma omp parallel shared(a,b,c,nthreads,chunk) private(i,tid)
    {
        tid = omp_get_thread_num();
        if (tid == 0)
        {
            nthreads = omp_get_num_threads();
            printf("Number of threads = %d\n", nthreads);
        }
        printf("Thread %d starting...\n", tid);

#pragma omp for schedule(dynamic,chunk)
        for (i = 0; i < N; i++)
        {
            c[i] = a[i] + b[i];
            printf("Thread %d: c[%2d]= %g\n", tid, i, c[i]);
        }

    } /* end of parallel section */
}

// -----------------------------------------------------------------------------

/*
 * DESCRIPTION:
 *   OpenMP Example - Sections Work-sharing - C Version
 *   In this example, the OpenMP SECTION directive is used to assign
 *   different array operations to each thread that executes a SECTION.
 * AUTHOR: Blaise Barney  5/99
 * LAST REVISED: 07/16/07
 */

void workshare2()
{
    printf("\n~~~~~~~~ workshare2\n");
    const int N = 50;

    int i, nthreads, tid;
    float a[N], b[N], c[N], d[N];

    /* Some initializations */
    for (i = 0; i < N; i++)
    {
        a[i] = i * 1.5;
        b[i] = i + 22.35;
        c[i] = d[i] = 0.0;
    }

#pragma omp parallel shared(a,b,c,d,nthreads) private(i,tid)
    {
        tid = omp_get_thread_num();
        if (tid == 0)
        {
            nthreads = omp_get_num_threads();
            printf("Number of threads = %d\n", nthreads);
        }
        printf("Thread %d starting...\n", tid);

#pragma omp sections nowait
        {
#pragma omp section
            {
                printf("Thread %d doing section 1\n", tid);
                for (i = 0; i < N; i++)
                {
                    c[i] = a[i] + b[i];
                    printf("Thread %d: c[%d]= %f\n", tid, i, c[i]);
                }
            }

#pragma omp section
            {
                printf("Thread %d doing section 2\n", tid);
                for (i = 0; i < N; i++)
                {
                    d[i] = a[i] * b[i];
                    printf("Thread %d: d[%d]= %f\n", tid, i, d[i]);
                }
            }

        } /* end of sections */

        printf("Thread %d done.\n", tid);

    } /* end of parallel section */
}

// -----------------------------------------------------------------------------

/*
 * DESCRIPTION:
 *   OpenMP Example - Combined Parallel Loop Reduction - C/C++ Version
 *   This example demonstrates a sum reduction within a combined parallel loop
 *   construct.  Notice that default data element scoping is assumed - there
 *   are no clauses specifying shared or private variables.  OpenMP will
 *   automatically make loop index variables private within team threads, and
 *   global variables shared.
 * AUTHOR: Blaise Barney  5/99
 * LAST REVISED: 04/06/05
 */
void reduction()
{
    int i, n;
    float a[100], b[100], sum;

    /* Some initializations */
    n = 100;
    for (i = 0; i < n; i++)
        a[i] = b[i] = i * 1.0;
    sum = 0.0;

#pragma omp parallel for reduction(+:sum)
    for (i = 0; i < n; i++)
        sum = sum + (a[i] * b[i]);

    printf("   Sum = %f\n", sum);
}

// -----------------------------------------------------------------------------

/*
 * DESCRIPTION:
 *   OpenMP Example - Parallel region with an orphaned directive - C/C++ Version
 *   This example demonstrates a dot product  being performed by an orphaned
 *   loop reduction construct.  Scoping of the reduction variable is critical.
 * AUTHOR: Blaise Barney  5/99
 * LAST REVISED: 06/30/05
 */

const int VECLEN = 100;
float a[VECLEN], b[VECLEN], sum;

float dotprod()
{
    int i, tid;

    tid = omp_get_thread_num();
#pragma omp for reduction(+:sum)
    for (i = 0; i < VECLEN; i++)
    {
        sum = sum + (a[i] * b[i]);
        printf("  tid= %d i=%d\n", tid, i);
    }
}

void orphan()
{
    int i;

    for (i = 0; i < VECLEN; i++)
        a[i] = b[i] = 1.0 * i;
    sum = 0.0;

#pragma omp parallel
    dotprod();

    printf("Sum = %f\n", sum);

}

// -----------------------------------------------------------------------------

/*
 * DESCRIPTION:
 *   OpenMp Example - Matrix Multiply - C Version
 *   Demonstrates a matrix multiply using OpenMP. Threads share row iterations
 *   according to a predefined chunk size.
 * AUTHOR: Blaise Barney
 * LAST REVISED: 06/28/05
 */

#define NRA 62                 /* number of rows in matrix A */
#define NCA 15                 /* number of columns in matrix A */
#define NCB 7                  /* number of columns in matrix B */

void matrixmultiply()
{
    int tid, nthreads, i, j, k, chunk;
    double a[NRA][NCA], /* matrix A to be multiplied */
        b[NCA][NCB], /* matrix B to be multiplied */
        c[NRA][NCB]; /* result matrix C */

    chunk = 10; /* set loop iteration chunk size */

    /*** Spawn a parallel region explicitly scoping all variables ***/
#pragma omp parallel shared(a,b,c,nthreads,chunk) private(tid,i,j,k)
    {
        tid = omp_get_thread_num();
        if (tid == 0)
        {
            nthreads = omp_get_num_threads();
            printf("Starting matrix multiple example with %d threads\n", nthreads);
            printf("Initializing matrices...\n");
        }
        /*** Initialize matrices ***/
#pragma omp for schedule (static, chunk)
        for (i = 0; i < NRA; i++)
            for (j = 0; j < NCA; j++)
                a[i][j] = i + j;
#pragma omp for schedule (static, chunk)
        for (i = 0; i < NCA; i++)
            for (j = 0; j < NCB; j++)
                b[i][j] = i * j;
#pragma omp for schedule (static, chunk)
        for (i = 0; i < NRA; i++)
            for (j = 0; j < NCB; j++)
                c[i][j] = 0;

        /*** Do matrix multiply sharing iterations on outer loop ***/
        /*** Display who does which iterations for demonstration purposes ***/
        printf("Thread %d starting matrix multiply...\n", tid);
#pragma omp for schedule (static, chunk)
        for (i = 0; i < NRA; i++)
        {
            printf("Thread=%d did row=%d\n", tid, i);
            for (j = 0; j < NCB; j++)
                for (k = 0; k < NCA; k++)
                    c[i][j] += a[i][k] * b[k][j];
        }
    } /*** End of parallel region ***/

    /*** Print results ***/
    printf("******************************************************\n");
    printf("Result Matrix:\n");
    for (i = 0; i < NRA; i++)
    {
        for (j = 0; j < NCB; j++)
            printf("%6.2f   ", c[i][j]);
        printf("\n");
    }
    printf("******************************************************\n");
    printf("Done.\n");
}

// -----------------------------------------------------------------------------

/*
 * DESCRIPTION:
 *   OpenMP Example - Get Environment Information - C/C++ Version
 *   The master thread queries and prints selected environment information.
 * AUTHOR: Blaise Barney  7/06
 * LAST REVISED: 07/12/06
 */
void getenvinfo()
{
    int nthreads, tid, procs, maxt, inpar, dynamic, nested;

    /* Start parallel region */
#pragma omp parallel private(nthreads, tid)
    {
        /* Obtain thread number */
        tid = omp_get_thread_num();

        /* Only master thread does this */
        if (tid == 0)
        {
            printf("Thread %d getting environment info...\n", tid);

            /* Get environment information */
            procs = omp_get_num_procs();
            nthreads = omp_get_num_threads();
            maxt = omp_get_max_threads();
            inpar = omp_in_parallel();
            dynamic = omp_get_dynamic();
            nested = omp_get_nested();

            /* Print environment information */
            printf("Number of processors = %d\n", procs);
            printf("Number of threads = %d\n", nthreads);
            printf("Max threads = %d\n", maxt);
            printf("In parallel? = %d\n", inpar);
            printf("Dynamic threads enabled? = %d\n", dynamic);
            printf("Nested parallelism supported? = %d\n", nested);

        }
    } /* Done */
}

// -----------------------------------------------------------------------------

int main(int argc, char *argv[])
{
//    hello();
    workshare1();
//    workshare2();
//    reduction();
//    orphan();
//    matrixmultiply();
    getenvinfo();
}
